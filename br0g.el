;;; br0g.el --- publish the br0g -*- lexical-binding: t; -*-

;; Copyright © 2021–2023  Hraban Luyat
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(require 'org)
(require 'ox-html)
(require 'ox-publish)

;; Allow loading from current directory
(let ((load-path (append load-path '(nil))))
  (require 'css-treeshake)
  (require 'b0-notes)
  (require 'b0-page))

(defun br0g//register-org (projects)
  "Extend ‘org-publish-project-alist with the given projects.

Replaces existing projects of the same name if found."
  (dolist (p projects)
    (cl-destructuring-bind (name . val) p
      (setf (alist-get name org-publish-project-alist nil nil 'equal) val))))

(br0g//register-org
 `(("br0g-main"
    :base-directory "src/org/main/"
    :publishing-directory "dist/"
    :publishing-function b0-page/publish-to-html
    :recursive t)
   ("br0g-notes"
    ;; Put this in org/ so the two can link to each other as org files and
    ;; publishing will automatically convert those links to .html files.
    :base-directory "src/org/notes"
    :publishing-directory "dist/notes"
    :publishing-function b0-notes/publish-to-html)
   ("br0g-static"
    :base-directory "src/static/"
    :publishing-directory "dist/"
    :base-extension "[^~]*"
    :include ("_redirects" "_headers")
    :recursive t
    :publishing-function org-publish-attachment)
   ("br0g"
    :components ( "br0g-main"
                  "br0g-notes"
                  "br0g-static"))))

(defun br0g/publish ()
  (interactive)
  (org-publish-project "br0g")
  (css-treeshake-build-dir "dist"))

(provide 'br0g)
