;; Copyright © 2021–2022  Hraban Luyat
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; An exercise in wasting time

(eval-when-compile
  (require 'cl-lib)
  (require 'dash)
  (require 'subr-x)
  (require 's))

(defun find-delims (start-delim end-delim)
  "Find the position of the next two delimiters, if they can be found"
  (save-excursion
    (when-let* ((pos (search-forward start-delim nil t))
                (start (- pos (length start-delim)))
                (end (search-forward end-delim nil t)))
      (list start end))))

(defun delete-between-next-delims (start-delim end-delim)
  "Delete the region between (including) these two delimiters.

Leaves the pointer where the region has just been deleted."
  (when-let ((res (find-delims start-delim end-delim)))
    (cl-destructuring-bind (start end) res
      (goto-char start)
      (delete-region start end)
      start)))

(defun remove-next-css-comment ()
  "Delete the next CSS comment and move to where it was"
  (interactive)
  (delete-between-next-delims "/*" "*/"))

(defun remove-css-comments ()
  (interactive)
  (while (remove-next-css-comment)))

(defun next-match (re bound)
  "Find the next regex match group as a string, or nil if none.

Bound is a point indicating a limit up to where to search.

This function DOES NOT preserve regex match data. In fact, it is intended to be
called repeatedly with the same regex, to find all matches of a capture group,
instead of just the first one."
  (when (re-search-forward re bound t)
    (match-string-no-properties 1)))

(defun find-all-groups-aux (re bound &optional all)
  (let ((res (next-match re bound)))
    (if res
        (find-all-groups-aux re bound (cons res all))
      all)))

(defun find-all-groups (re bound)
  "Find ALL matches of the first capture group in this regex"
  (save-excursion
    (save-match-data
      (find-all-groups-aux re bound))))

;; This falls apart on :not(..) 🤷‍♀️
(defun css-ts//find-classes-sub (s &optional res)
  (if (string-match (rx "." (group (1+ (any "a-z" "A-Z" "0-9" "-" "_")))) s)
      (css-ts//find-classes-sub (substring s (match-end 0)) (cons (match-string 1 s) res))
    res))

(defun css-ts//find-classes-str (s)
  "Find all CSS classes used by this rule in S.

Returns a list of OR-able terms. E.g.:

(css-ts//find-classes-str \"hey.foo.bar, .yala, .yes zim\")
=> ((\"foo\" \"bar\") (\"yala\") (\"yes\")) ;; order not guaranteed

Note that for this rule to apply, only one of the top-level lists
needs to apply (.foo.bar, .yala, OR .yes), but ALL of the terms
in that one list do.
"
  (mapcar #'css-ts//find-classes-sub (string-split s ",")))

(defun find-classes (next-brace-pos)
  "Find all CSS classes used by the next rule"
  (css-ts//find-classes-str (buffer-substring-no-properties (point) next-brace-pos)))

(defun find-next-opening-brace ()
  "Find position of next {, leave cursor at last }.

This helps avoid jumping over nested closing braces, e.g. at the end of a media
query. If the next rule ends up being purged, it would also (spuriously) purge
the outer }."
  (let ((bracepos (save-excursion (re-search-forward "[{}]" nil t))))
    (when bracepos
      (if (equal "{" (match-string-no-properties 0))
          bracepos
        (goto-char bracepos)
        (find-next-opening-brace)))))

(defun next-rule ()
  "Find the next rule (if any), return it, and move to its end.

N.B.: Doesn't work with disjunctions aka `,'!

Another edge case is nested braces, but afaik that only happens with media
queries, which would not match any class, and therefore always end up being
included, along with their first member. This is a false positive, which is
fine."
  (interactive)
  (save-match-data
    (let ((bracepos (find-next-opening-brace)))
      (when bracepos
        (list :start (point)
              :classes (find-classes bracepos)
              ;; This would actually be an error if not found.
              :end (search-forward "}"))))))

(defun subset-p (seq hash-table)
  "Return non-nil if every el in the seq exists in the hash-table"
  (cl-every (lambda (x) (gethash x hash-table)) seq))

(defun keep-reachable-rules (used-classes)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (cl-destructuring-bind (&key start end classes) (next-rule)
               (when start
                 ;; just look for unused classes. Anything else I don’t care
                 ;; about for now. The major purpose of this tool is pruning
                 ;; default org-mode style export which is 90% unused, and all
                 ;; uses classes.
                 (unless (--any? (subset-p it used-classes) classes)
                   (delete-region start end))
                 t))))))

(defun find-classes-dom (dom callback)
  (when (consp dom)
    (if (eq (car dom) 'class)
        (funcall callback (cdr dom))
      (find-classes-dom (car dom) callback)
      (find-classes-dom (cdr dom) callback))))

(defun find-classes-buf (callback)
  ;; The default is too low.
  (let ((max-lisp-eval-depth 10000))
    (find-classes-dom (libxml-parse-html-region (point-min) (point-max))
                      callback)))

(defun find-classes-file (callback fname)
  (with-temp-buffer
    (insert-file-contents fname)
    (find-classes-buf callback)))

(defun find-classes-dir (dir callback)
  (->> "\\.html$"
       (directory-files-recursively dir)
       (mapc (-partial 'find-classes-file callback))))

(defun collect-classes-dir (dir)
  (let ((all (make-hash-table :test 'equal)))
    (cl-flet* ((add-cls (cls)
                 (puthash cls t all))
               (add-classes (clsstr)
                 (mapc #'add-cls (split-string clsstr " "))))
      (find-classes-dir dir #'add-classes))
    all))

(defun css-treeshake-buffer (used-classes)
  (remove-css-comments)
  (keep-reachable-rules used-classes))

(defun css-minified-name (fname)
  (-> fname file-name-sans-extension (concat ".min.css")))

(defun css-treeshake-file (used-classes fname)
  (with-temp-buffer
    (insert-file-contents fname)
    (css-treeshake-buffer used-classes)
    (write-file (css-minified-name fname))))

(defun css-treeshake-dir (dir used-classes)
  ;; Leave the original full stylesheet in place, it’s useful to have it online
  ;; in case I need it from another project. I wouldn’t want the tree shaken
  ;; version in that case.
  (->> "\\.css$"
       (directory-files-recursively dir)
       (-remove (-partial 's-ends-with? ".min.css"))
       (mapc (-partial 'css-treeshake-file used-classes))))

(defun css-treeshake-build-dir (dir)
  (interactive "D")
  "Find all HTML files and CSS files in this dir and treeshake"
  (css-treeshake-dir dir (collect-classes-dir dir)))

(provide 'css-treeshake)
