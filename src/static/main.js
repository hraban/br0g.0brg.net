/* UGGHHGHHGHGHHHERHJHGKSDHFLKASDUfh */

function loadScript(url) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    script.defer = true;
    script.async = true;
    document.head.appendChild(script);
}

// https://stackoverflow.com/a/39106105/4359699
function hasMathMLSupport() {
    const div = document.createElement("div");
    div.innerHTML = '<math><mspace height="20px" width="20px"></mspace></math>';
    document.body.appendChild(div);
    const supports = div.firstChild.firstChild.getBoundingClientRect().height === 20;
    document.body.removeChild(div);
    return supports;
}

function pageHasMathML() {
    return document.getElementsByTagName('math').length > 0;
}

function main () {
    if (pageHasMathML() && !hasMathMLSupport()) {
        loadScript('/mathjax/tex-mml-chtml.js');
    }
}

main();
