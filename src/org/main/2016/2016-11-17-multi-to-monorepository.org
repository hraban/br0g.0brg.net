#+title: Multi- to Monorepository
#+date: 2016-11-17
#+copyright_years: 2016
#+export_file_name: multi-to-monorepository.html

#+begin_quote
Update: this tool has been [[../2022/multi-to-monorepository-literate-programming.html][rewritten in Literate Programming]].
#+end_quote

Managing all those separate Git repositories for your microservices architecture driving you crazy? Dream of a day where you can migrate it all into one big MonoRepo, like Google?

Fear not! I wrote a tool, [[https://github.com/hraban/tomono][tomono]], and accompanying [[https://syslog.ravelin.com/multi-to-mono-repository-c81d004df3ce][explanatory blog post]] (for Ravelin) which helps you do just that.

#+attr_html: :alt Multi- to Monorepository sketch :width 600px :src /img/2016-11-17-tomono-banner.png
[[../../static/img/2016-11-17-tomono-banner.png]]

P.S.: [[https://www.ravelin.com/][Ravelin]] is an awesome London-based tech company, and they're always hiring.
