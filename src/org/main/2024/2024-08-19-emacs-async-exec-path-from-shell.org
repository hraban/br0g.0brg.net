#+title: Asynchronously set PATH in Emacs
#+date: 2024-08-19
#+copyright_years: 2024
#+export_file_name: emacs-async-exec-path-from-shell.html

Copied from my =~/.emacs.d/init.org=:

* Set PATH & other envvars

** Synchronously

=exec-path-from-shell= loads my .bashrc to load the full PATH. However, within it, I want to behave differently based on whether it's loaded from Emacs, or actually for a real interactive shell (notably: no lazy loading PATHs). This helps me detect in =.bashrc= that it's actually being loaded from Emacs.

#+begin_src emacs-lisp
(defun hly/set-path-sync ()
  (when (and (fboundp 'exec-path-from-shell-initialize)
             (memq window-system '(mac ns x)))
    (setenv "INSIDE_EMACS" "true")
    (exec-path-from-shell-initialize)
    (hly//call-path-set-hook)))
#+end_src

I don't know how conventional this envvar name is. It is featured in Mac OS'es stock =/etc/bashrc_Apple_Terminal= init script, so that's something.

** Asynchronously

Default =exec-path-from-shell= is sync and slow as hell.  We can make it async, at the cost of not having the PATH set correctly right away after the call returns.

#+begin_src emacs-lisp
(defun hly/set-path-async ()
  (async-start
   `(lambda ()
      ;; https://github.com/NixOS/nixpkgs/issues/237855
      ,(async-inject-variables "load-path")
      ;; Necessary for ‘exec-path-from-shell-variables’
      (load "~/.emacs.d/customizations.el")
      (package-initialize)
      (require 'exec-path-from-shell)
      (setenv "INSIDE_EMACS" "true")
      (exec-path-from-shell-initialize))
   (lambda (pairs)
     (mapc (lambda (pair)
             (exec-path-from-shell-setenv (car pair) (cdr pair)))
           pairs)
     (hly//call-path-set-hook))))

(if (and (require 'exec-path-from-shell nil nil)
         (require 'async nil nil))
    (hly/set-path-async)
  (hly/set-path-sync))
#+end_src

** Hooks for functions to call only with PATH

Some functions need the PATH set, which needs a hook when using async:

#+begin_src emacs-lisp
(setq hly//after-path-set-hook ())

(defun hly//on-path-set (f)
  "Call F when path is set"
  (if (eq 'done hly//after-path-set-hook)
      (funcall f)
    (push f hly//after-path-set-hook)))

(defun hly//call-path-set-hook ()
  (mapc #'funcall hly//after-path-set-hook)
  (setq hly//after-path-set-hook 'done))
#+end_src

Use like this:

#+begin_src emacs-lisp
(hly//on-path-set (lambda ()
                    (when (require 'direnv nil t)
                      (direnv-mode))))
#+end_src

(Now I’m actually doubting if this works---is this executed by the async worker or by main Emacs?? It works here but I wonder if that’s just by accident.)
