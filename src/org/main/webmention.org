#+title: Webmention submission received
#+date: 2023-08-17
#+copyright_years: 2023
#+export_file_name: webmention.html
#+b0_page: page

Thank you for your submission.
