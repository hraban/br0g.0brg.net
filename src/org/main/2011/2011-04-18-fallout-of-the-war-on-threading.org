#+title: Fallout of The War On Threading
#+date: 2011-04-18
#+copyright_years: 2011
#+export_file_name: fallout-of-the-war-on-threading.html

A recurring pattern: a young developer is slowly getting to grips with programming in language X, writing his first big program, Y. This is not his first language, but it is the first language in which he is making some real headway: a big program, different modules, actual OOP, etcetera. Chances are, X is whatever the flavour du jour is for beginning programmers (Java, Python, …), and Y is a chatbot. At one point, he wants to do two things at the same time, and in some shady forum, somebody with a loosely related problem got the advice to "use threading lol". Thus, he steps into the dreary realm of multi-threading, haunted by the ghost of Heisenbug.

For a while, this solves his problem, and all seems good. Wow, you can do multiple things at once! But it is not long before the first bug bites him. As he joins the support channel, he mentions that he comes from the land of threading. The seasoned programmers are quick to let him know, they do not serve his kind around here. Threading is bad, threading is evil, boo, threading, boo.

And they are right: threading (as found in our most popular languages) exposes you to a world of hurt and while getting them right is hard enough already, knowing whether you actually got it right is near-impossible. But.

All to quickly, this argument flows into a “use select()“, “polling is the way”. This is not OK. We are too quick to use this as an excuse to not attempt any kind of concurrency and just leave it at that. Do this, and you are forever left in the land of unresponsive GUIs, where one silly subtask gone wrong can send your entire program into a deep sleep. This is unacceptable: we have traveled too far and fought too hard to allow this obsession with predictability and correctness to just throw away any chance we have at interactive programs.

Remember how your entire computer would lock up for a few seconds when you accidentally clicked A:? Why was that ever acceptable? Did nobody in QA catapult their test unit from Redmond into space out of frustration with this unnecessary power such frail things as an open() syscall have on something as important as explorer.exe? Apparently not. Thanks for being understanding. Now we are stuck with your patience.

It is time to stop accepting frozen GUIs. Let customer demand drive the programming languages that support concurrency paradigms best to the front row.

No excuses!
