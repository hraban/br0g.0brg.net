#+title: Use literal λ (lambda) character in Common Lisp
#+date: 2011-07-31
#+copyright_years: 2011
# Netlify has problems with utf-8 filenames in zip archives. Time to migrate?
#+export_file_name: literal-lambda-in-lisp.html

Thanks to [[http://www.bookshelf.jp/texi/onlisp/onlisp_18.html][On Lisp]] for the details:

#+begin_src lisp
(set-macro-character #\λ (lambda (s c) 'lambda))
#+end_src

Allows you to write prettier lambdas in Common Lisp:

#+begin_src lisp
(mapcar (λ (x) (* 5 x)) '(1 2 3 4))
#+end_src

#+RESULTS:
: (5 10 15 20)

Unlike the =(defmacro λ ...)= you can really use it everywhere you can use lambda. For example, anonymous recursion:

#+begin_src lisp
(funcall
  ((λ (x) (λ (&rest y) (apply x x y)))
    (λ (f x) (if x (+ (first x) (funcall f f (rest x)))
                   0)))
  '(1 2 3 4))
#+end_src

#+RESULTS:
: 10

