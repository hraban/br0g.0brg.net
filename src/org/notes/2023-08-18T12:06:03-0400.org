Maybe this person doesn’t know what a robots.txt is or how it works, and just assumed it was more like a firewall that autodetects the bot?

* Cite
:PROPERTIES:
:MF2_AUTHOR_NAME: Gregor Morrill
:MF2_AUTHOR_URL: https://gregorlove.com/
:MF2_CITATION_URL: https://gregorlove.com/2023/08/i-got-a-spammy/
:MF2_CITATION_DATE: 2023-08-07T11:18-0700
:END:

I got a spammy message via my contact page from “PostBy AI” trying to sell me on adding a chatbot to my site to answer customer questions, generate le…
