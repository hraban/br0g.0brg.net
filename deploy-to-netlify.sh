#!/usr/bin/env bash

set -eu -o pipefail

branch="${1+branch=$1}"

exec curl -sSL --fail \
     -H "Content-Type: application/zip" \
     -H "Authorization: Bearer $NETLIFY_AUTH_TOKEN" \
     --data-binary "@${SITE_ZIP}" \
     "https://api.netlify.com/api/v1/sites/$NETLIFY_SITE/deploys?${branch}"
