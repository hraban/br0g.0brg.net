# Copyright © 2022–2024  Hraban Luyat
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

{
  inputs = {
    nixpkgs.url = "nixpkgs";
    systems.url = "systems";
    gitignore = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:hercules-ci/gitignore.nix";
    };
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;
      perSystem = { config, self', inputs', pkgs, system, ... }:
        with rec {
          cleanSource = src: pkgs.lib.cleanSource (inputs.gitignore.lib.gitignoreSource src);
        };
        {
          packages = {
            site = pkgs.stdenv.mkDerivation {
              name = "br0g.0brg.net";
              src = cleanSource ./.;
              buildInputs = [
                self'.packages.latexml
              ];
              buildPhase = ''
                # Org and stuff and too many packages are just rude about caching.
                HOME="$PWD" ${self'.packages.emacs}/bin/emacs --script publish.el
              '';
              installPhase = ''
                cp -r dist "$out"
              '';
            };
            site_zip = pkgs.stdenv.mkDerivation {
              name = "br0g.0brg.net-zip";
              src = self'.packages.site;
              buildPhase = "${pkgs.zip}/bin/zip -r site.zip .";
              installPhase = "cp site.zip $out";
            };

            #
            # Internal tools exposed externally for dev.
            #

            # emacs 29 has cl-with-gensyms
            emacs = (pkgs.emacsPackagesFor pkgs.emacs29).emacsWithPackages (epkgs: with epkgs; [
              dash
              htmlize
              nix-mode # For highlighting
              s
              xmlgen
            ]);
            # Need 3.11 for datetime.datetime.fromisoformat. Expose as package for
            # development.
            python = pkgs.python311.withPackages (ps: with ps; [bunch mf2py]);
            # 🤷
            mf2py = pkgs.writeScriptBin "mf2py" ''
              #!${self'.packages.python}/bin/python3
              ${(builtins.readFile ./mf2py.py)}
            '';
            netlify-deploy = pkgs.writeShellApplication {
              name = "netlify-deploy";
              runtimeInputs = [ pkgs.curl ];
              text = builtins.readFile ./deploy-to-netlify.sh;
            };
            latexml = pkgs.stdenvNoCC.mkDerivation {
              name = "latexml";
              src = pkgs.perl536Packages.LaTeXML;
              dontUnpack = true;
              nativeBuildInputs = [ pkgs.makeWrapper ];
              installPhase = ''
                mkdir -p $out/bin
                cd $src/bin
                for f in *; do
                  # latexml gets confused by the environment's SHELL and
                  # prints noise to stdout (yuck)
                  makeWrapper $src/bin/$f $out/bin/$f --unset SHELL
                done
              '';
            };
            css-treeshake = let
              el = pkgs.writeText "css-treeshake-exec.el" ''
                (load "${./css-treeshake.el}")
                (setf help "Usage: nix run gitlab:hraban/br0g.0brg.net#css-treeshake -- DIRECTORY")
                (pcase (cdr command-line-args-left)
                  (`(,(or "--help" "-h"))
                   (message "%s" help))
                  (`(,dir)
                   (css-treeshake-build-dir dir))
                  (_
                   (message "%s" help)))
                (kill-emacs)
              '';
              in pkgs.writeShellScriptBin "css-treeshake" ''
                exec ${self'.packages.emacs}/bin/emacs --script ${el} -- "$@"
              '';
            default = self'.packages.site;
          };
          devShells = {
            default = pkgs.mkShell {
              nativeBuildInputs = [
                self'.packages.mf2py
                self'.packages.python
                self'.packages.emacs
              ];
              inputsFrom = [
                self'.packages.site
              ];
            };
          };
        };
    };
}
