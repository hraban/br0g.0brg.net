import mf2py
import sys
arg = sys.argv[1] if len(sys.argv) > 1 else '-'
f = sys.stdin if arg == '-' else open(arg)
print(mf2py.Parser(doc=f).to_json())
