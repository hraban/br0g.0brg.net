;;; b0-notes --- microformats2 notes in org -*- lexical-binding: t; -*-

;; Copyright © 2023  Hraban Luyat
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Publish notes, a bespoke micro-blogging-in-org export handler

(require 'cl-lib)
(require 'url-expand)
(require 'xmlgen)

(eval-when-compile
  (let ((load-path (append load-path '(nil))))
    (require 'b0-page)))

(defmacro b0-notes//with-temp-buf-bg (buf &rest body)
  "Like ‘with-temp-buffer’ but stays in position.

Assigns the variable BUF to a reference to the temporary buffer.
"
  (declare (indent defun))
  (cl-with-gensyms (mainbuf)
    `(let ((,mainbuf (current-buffer)))
       (push-mark)
       (with-temp-buffer
         (let ((,buf (current-buffer)))
           (switch-to-buffer ,mainbuf)
           (goto-char (mark-marker))
           (pop-mark)
           ,@body)))))

(defun b0-notes//rcurry (f &rest rargs)
  (lambda (&rest args) (apply f (append args rargs))))

(defun b0-notes//export-file-to-str (path info)
  (with-temp-buffer
    (insert-file-contents path)
    (b0-notes//with-temp-buf-bg htmlbuf
      ;; The note contents are pure HTML, don’t actually do any note
      ;; processing. That’s all done in the feed itself, because I want
      ;; different resulting wrapping HTML (e.g. footer above vs below).
      (org-export-to-buffer 'html htmlbuf
        nil nil nil t
        ;; This should apply to inline notes
        '(:filter-parse-tree (b0-notes//build-citations)))
      (switch-to-buffer htmlbuf)
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun b0-notes//fmt-date (iso)
  (let* ((dec (iso8601-parse iso))
         (enc (encode-time dec))
         (tz (nth 6 dec)))
    (format-time-string "%b %e, %Y" enc tz)))

(defun b0-notes/note-url (date info)
  "Given a date (bare filename) and info block, this is the full URL"
  (b0-page//url-join (plist-get info :html-link-home)
                     (plist-get info :html-link-up)
                     (format "%s.html" date)))

(defun b0-notes//author-and-date (a-name a-url p-date p-url)
  "xmlgen compatible author & date for a post."
  `(footer :class "author-and-date"
           (a :class "p-author h-card"
              :href ,a-url
              ,a-name)
           " · "
           (a :class "u-url" :href ,p-url
              (time :class "dt-published" :datetime ,p-date
                    ,(b0-notes//fmt-date p-date)))))

(defun b0-notes//my-signature (date info)
  (b0-notes//author-and-date (org-element-interpret-data (plist-get info :author))
                             (plist-get info :html-link-home)
                             date
                             (b0-notes/note-url date info)))

(defun b0-notes//export-note (fname info)
  "Export the note at this FNAME as xmlgen compatible markup.

The file name should be a valid date."
  (let ((date (file-name-base fname))
        (html (b0-notes//export-file-to-str fname info)))
    `(article :class "h-entry note"
              (section :class "e-content"
                       (!unescape ,html))
              ;; HACK -- this takes the index’s info and passes it as the note’s
              ;; info. Good for some stuff (:html-link-*), bad for others
              ;; (:author), but for now who cares. Just keep it in mind.
              ,(b0-notes//my-signature date info))))

(b0-page//define-replacer b0-notes//build-index "NOTES_INDEX" (info)
  "Convert a list of notes to a HTML feed"
  (let ((notes (b0-notes//find-notes)))
    (with-temp-buffer
      (dolist (note notes)
        (insert (xmlgen (b0-notes//export-note note info))))
      ;; Why not 🤷
      (insert (xmlgen `(div :style "display: none;"
                            (a :class "u-url" :href "https://br0g.0brg.net/notes.html" "")
                            (p :class "p-name"
                               "Hraban’s Notes")
                            (a :class "h-card u-author" :href "https://br0g.0brg.net"
                               "Hraban"))))
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun b0-notes//find-notes ()
  "Find all note files in the current directory"
  (->> (rx (= 4 digit) "-" (= 2 digit) "-" (= 2 digit) (* anything) ".org" eol)
       (directory-files "." nil)
       ;; Newest first
       reverse))

(defun b0-notes//inner-template (contents info)
  "Body HTML generation.

ox-html considers the body to be only the content of the
container, not including:

- the wrapping container div itself
- the postamble / preamble
- the up/down links

This is different from a Microformats2 h-entry, which wants to
contain everything, including the author etc. So I don’t use
ox-html’s postamble nor preamble, and just generate the whole
thing to live within the wrapping container div (which I give
class h-entry).
"
  (pcase-exhaustive (plist-get info :b0-page)
    ("note"
     (b0-page//xmlgens
      `(,(b0-notes//my-signature (b0-notes//filename-base info) info)
        (section :class "e-content" (!unescape ,contents)))))
    ("page"
     contents)))

(defun b0-notes//export-org-subtree-to-str (point)
  (save-excursion
    (goto-char point)
    (b0-notes//with-temp-buf-bg htmlbuf
      (org-export-to-buffer 'html htmlbuf
        nil t nil t nil)
      (switch-to-buffer htmlbuf)
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun b0-notes//build-citations (ast backend info)
  "Turn all * Citation ... blocks into HTML citation of specific format"
  (org-element-map ast 'headline
    (lambda (hl)
      (b0-page//with-element-properties ( raw-value level begin end
                                          MF2_AUTHOR_NAME
                                          MF2_AUTHOR_URL
                                          MF2_CITATION_URL
                                          MF2_CITATION_DATE)
          hl
        (when (and (equal "Cite" raw-value)
                   (equal 1 level))
          (b0-page//replace-with-html
           hl
           (xmlgen
            `(article :class "u-in-reply-to h-cite"
                      (blockquote :class "p-content" :cite ,MF2_CITATION_URL
                                  (!unescape ,(b0-notes//export-org-subtree-to-str begin)))
                      ,(b0-notes//author-and-date MF2_AUTHOR_NAME
                                                  MF2_AUTHOR_URL
                                                  MF2_CITATION_DATE
                                                  MF2_CITATION_URL)))))))
    info t)
  ast)

(defun b0-notes//filename-base (info)
  (-> info
      (plist-get :path)
      file-name-base))

(defun b0-notes//filter-parse-tree (ast backend info)
  "Build the index and/or any citations"
  (-> ast
      (b0-notes//build-index backend info)
      (b0-notes//build-citations backend info)))

(defun b0-notes//filter-options (options _backend)
  "Change the options plist before doing actual export.

This allows us to reuse a lot of the existing HTML export code.
"
  `( :html-home/up-format ,(xmlgen '(nav :id "org-div-home-and-up"
                                         (a :href "%s" "Notes")
                                         " | "
                                         (a :href "%s" "Home")))
     :html-link-up "/notes/"
     ,@ (pcase-exhaustive (plist-get options :b0-page)
          ("note"
           `( :html-content-class "h-entry note"
              :html-divs (;; In a single note page, the content = the article
                          (content "article" "content")
                          (preamble "div" "preamble")
                          (postamble "div" "postamble"))
              :title ,(->> options
                           b0-notes//filename-base
                           b0-notes//fmt-date
                           (format "Hraban’s note, %s")))  )
          ("page"
           `( :html-content-class "h-feed"
              :html-divs (;; In the feed, the content is a big list of all the
                          ;; notes
                          (content "main" "content")
                          (preamble "div" "preamble")
                          (postamble "div" "postamble")))))
     . ,options))

(defun b0-notes/publish-to-html (plist filename pub-dir)
  "Publishing function for notes"
  (b0-page/publish-to-html plist filename pub-dir 'b0-notes))

(org-export-define-derived-backend 'b0-notes 'b0-page
  ;; Default to note so a note can truly be a simple text file without markup
  :options-alist '((:b0-page "B0_PAGE" nil "note"))
  :filters-alist '((:filter-options . b0-notes//filter-options)
                   (:filter-parse-tree . b0-notes//filter-parse-tree))
  :translate-alist '((inner-template . b0-notes//inner-template))
  ;; Most of this is boiler plate copied from ox-html
  :menu-entry '(?n "Export as MF2 Notes"
                   ((?n "As HTML file"
                        (lambda (&optional a s v b p)
                          (let* ((file (org-export-output-file-name ".html" s))
	                         (org-export-coding-system org-html-coding-system))
                            (org-export-to-file 'b0-notes file a s v b p))))
                    (?N "as HTML buffer"
                        (lambda (&optional a s v b p)
                          (org-export-to-buffer 'b0-notes "*Org b0-notes HTML Export*"
                            a s v b p
                            (lambda () (set-auto-mode t))))))))

(provide 'b0-notes)
