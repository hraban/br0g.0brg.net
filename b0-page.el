;;; b0-page.el --- br0g’s HTML pages -*- lexical-binding: t; -*-

;; Copyright © 2021–2023  Hraban Luyat
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, version 3 of the License.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Create an HTML derived backend which outputs the HTML that I like by
;; hard-coding my favourite options, and hard-coding HTML template generation as
;; I like it. ox-html tries to be everything to everyone, allowing people to
;; customize the generated HTML through options, and achieving that goal mostly
;; through string replacement. I find it opaque. It’s much easier for me to just
;; create a template the way I like it and call it a day.
;;
;; ox-html shines when you want to create your *inner* content: say you have a
;; heading and its subcontents, which contain latex, bold text, etc: ox-html is
;; great for that part. But you don’t want to rely on its final HTML
;; page. Rather, just take that subtree, and shove it into your own root
;; template.

(require 'htmlize)
(require 's)

(require 'org)
(require 'ox-publish)
(require 'ox-html)

;; https://alxgbsn.co.uk/2011/10/17/enable-css-active-pseudo-styles-in-mobile-safari/
;; Tap on src blocks on mobile to hide the language label. Don’t know how to do
;; this from async loaded main.js, don’t care to find out right now.
;; UPDATE: I’m not sure this even works at all
(defvar b0-page//footer "<script async defer src=/main.js></script>
<script>document.addEventListener(\"touchstart\", function() {},false);</script>")

(defun b0-page//call (program &rest args)
  "Call PROGRAM with ARGS, and the current buffer as its stdin.

Deletes the entire input and replaces it with the output.  This
is only for use in a (with-temp-buffer ..) call.

(with-temp-buffer
  (insert \"foo bar baz\")
  (b0-page//call \"sed\" \"s/bar/quux/\")
  (buffer-string))
=> \"foo quux baz\"

This is comparable to vim’s :%! cmd args..

If the program exits with a non-0 status code, signal an error
instead.  Does not spawn a shell.  Utility wrapper around
‘call-process’, see that function for info on STDIN."
  (unless (eq 0 (apply #'call-process-region (point-min) (point-max) program t (current-buffer) nil args))
    (error "command %S failed: %s" program (current-buffer))))

(defun b0-page//remove-last-newline ()
  "Remove the last newline from the current buffer, if any.

(with-temp-buffer
  (insert \"\nfoo\n\")
  (b0-page//remove-last-newline)
  (buffer-string))
=> \"\nfoo\"

(with-temp-buffer
  (insert \"\nonly remove one\n\n\n\")
  (b0-page//remove-last-newline)
  (buffer-string))
=> \"\nonly remove one\n\n\"

(with-temp-buffer
  (insert \"foobar\")
  (b0-page//remove-last-newline)
  (buffer-string))
=> \"foobar\"

There has to be a better way?"
  (save-excursion
    (goto-char (point-max))
    (when (looking-at "^$")
      (delete-char -1))))

(defmacro b0-page//with-element-properties (props el-exp &rest body)
  (declare (indent 2))
  ;; Use a macro because we want the (soft) interning to happen not in the macro
  ;; evaluator but in the main compilation phase, after evaluating the element
  ;; value itself.
  (cl-macrolet ((symbol->kw (sym)
                  `(intern (format ":%s" ,sym))))
    (cl-with-gensyms (el-val)
      `(let* (;; Ensure we evaluate the element expression only once
              (,el-val ,el-exp)
              ,@ (mapcar (lambda (prop)
                           `(,prop
                             (org-element-property ,(symbol->kw prop)
                                                   ,el-val)))
                         props))
         ,@body))))

;; Janky memoization. Works for me.
(defvar b0-page//latex-html (make-hash-table :test #'equal))

(defmacro b0-page//get-or-set (key hash-table value)
  `(or (gethash ,key ,hash-table)
       (puthash ,key ,value ,hash-table)))

(defun b0-page//latex-as-html (latex-fragment)
  "Safe substitute for ‘org-format-latex-as-html’."
  (b0-page//get-or-set
   latex-fragment b0-page//latex-html
   (with-temp-buffer
     (insert latex-fragment)
     (b0-page//call "latexmlmath" "--strict" "-")
     (goto-char (point-min))
     ;; It also includes a newline at the end of the output which messes with the
     ;; HTML (e.g. it will put a space between the equation and possible
     ;; following punctuation).
     (b0-page//remove-last-newline)
     (buffer-string))))

(defun b0-page//inner-template-article (contents info date)
  ;; In Microformats2 the article related footer (author & date) must live
  ;; /inside/ the content, so I can’t use ox-html’s postamble stuff because
  ;; that’s fundamentally neighboring the content. It’s a big string
  ;; replace mess anyway, might as well just have everything in a clean
  ;; template all in one place.
  `( (header (h1 :class "title p-name" ,(org-export-data (plist-get info :title) info)))
     ,@ (-some--> :with-toc
          (plist-get info it)
          (org-html-toc it info)
          `(nav (!unescape ,it))
          ;; Wrap once more because we’re in a ,@
          list)
     (div :class "e-content"
          (!unescape ,contents))
     (footer
      (p "Date: "
         (a :class "u-url" :href ,(plist-get info :b0-page-url)
            (time :class "dt-published" :datetime ,date ,date)))
      (p "Copyright © " ,(plist-get info :copyright-years) " "
         (a :class "p-author h-card" :href ,(plist-get info :html-link-home)
            ,(plist-get info :author))))))

(defun b0-page//inner-template-page (contents info date)
  `( (header (h1 :class "title" ,(org-export-data (plist-get info :title) info)))
     ,@ (-some--> :with-toc
          (plist-get info it)
          (org-html-toc it info)
          `(nav (!unescape ,it))
          ;; Wrap once more because we’re in a ,@
          list)
     (!unescape ,contents)
     (footer
      (p "Date: "  ,date)
      (p "Copyright © " ,(plist-get info :copyright-years) " " ,(plist-get info :author)))))

(defun b0-page//xmlgens (elems)
  "Like ‘xmlgen’ but support multiple top-level elements"
  (apply #'concat (mapcar #'xmlgen elems)))

(defun b0-page//inner-template (contents info)
  "Generate HTML content"
  (let ((date (org-export-data (org-export-get-date info "%Y-%m-%d") info))
        (f (pcase-exhaustive (plist-get info :b0-page)
             ("article"
              #'b0-page//inner-template-article)
             ("page"
              #'b0-page//inner-template-page))))
    (b0-page//xmlgens (funcall f contents info date))))

(defun b0-page//url-join (&rest parts)
  (pcase parts
    (`() nil)
    (`(,p) p)
    (`(,p1 ,p2 . ,rest)
     ;; O(n²). Does it matter? No.
     (apply #'b0-page//url-join
            (concat
             (replace-regexp-in-string "/*$" "" p1)
             "/"
             (replace-regexp-in-string "^/*" "" p2))
            rest))))

(defun b0-page//translate-latex (latex contents options)
  (b0-page//latex-as-html (org-element-property :value latex)))

(defun b0-page//strip-prefix (s prefix)
  "Ensure S starts with PREFIX and return the part of S without it"
  (unless (equal prefix (substring s 0 (length prefix)))
    (user-error "%S does not start with expected prefix %S" s prefix))
  (substring s (length prefix)))

(defun b0-page//alist-replace (alist newprops)
  "Destructively replace NEWPROPS in ALIST.

Returns the final alist."
  (if newprops
      (pcase-let ((`((,key . ,val) . ,rest) newprops))
        (setf (alist-get key alist) val)
        (b0-page//alist-new alist rest))
    alist))

(defun b0-page//alist-new (alist newprops)
  "Non-destructively replace NEWPROPS in ALIST.

Returns the new alist."
  (b0-page//alist-replace (copy-tree alist) newprops))

(defun b0-page//replace-with-html (elt html)
  "Replace this org-element node in its AST with the given HTML"
  (org-element-set-element
   elt
   (org-element-create 'export-block `(:type "HTML" :value ,html))))

(defmacro b0-page//define-replacer (name keyword args &rest html-form)
  "Make function to replace a KEYWORD with html from HTML-FORM.

Keywords are of the shape:

#+MY_KEY:

Note the trailing colon.

The macro expands to a defun, which creates a function with the
given NAME. This function has an API conforming to the
‘org-export-filter-parse-tree-functions’ hook. The ARGS passed to
this macro must be one single argument: the communication
channel, as a plist. The created function will get three
arguments, but that is an implementation detail. It is expected
to return a raw HTML string.

This is the best solution I found which:

- is cleanly parsable in the AST, which makes it

- usable from an org export filter, instead of a hook, which
  means it

- can be implemented in the export backend configuration, instead
  of the publish function

Which is important if I want this to work on regular export, not
just full publish.
"
  (declare (indent defun))
  (cl-with-gensyms (ast backend elt)
    (cl-destructuring-bind (info) args
      `(defun ,name (,ast ,backend ,info)
         (ignore ,backend)
         (org-element-map ,ast 'keyword
           (lambda (,elt)
             (b0-page//with-element-properties (key) ,elt
               (when (equal key ,keyword)
                 (b0-page//replace-with-html ,elt (progn ,@html-form)))))
           ,info t)
         ,ast))))

(defun b0-page//postamble (info)
  (b0-page//xmlgens
   `((hr)
     (form :class "webmention-form"
           :action "/webmention"
           :name "webmention"
           :method "POST"
           :data-netlify "true"
           (input :type "hidden"
                  :name "target"
                  :value ,(plist-get info :b0-page-url))
           (label
            "To comment, publish online and post the URL here:"
            (br)
            (input :type "url" :name "source"))
           " "
           (button "Go"))
     (br) "Or leave a comment below:" (br)
     (form :action "/comment"
           :name "comment"
           :method "POST"
           :data-netlify "true"
           (textarea :rows 10 :cols 50 "" :name "comment")
           (br)
           (label "Name: "
                  (input :name "name"))
           (button "Say it")))))

(defun b0-page//filter-options (options _backend)
  (let* ((home "https://br0g.0brg.net")
         ;; Disable comments on root page(s)
         (webmentionp (not (equal "page" (plist-get options :b0-page))))
         (url (b0-page//url-join
               home
               (plist-get options :b0-publishing-subdir)
               (or (file-name-directory
                    (b0-page//strip-prefix (plist-get options :input-file)
                                           (plist-get options :b0-src-base)))
                   ;; In case the file is local to the current directory,
                   ;; which would yield nil, which no es bueno.
                   "")
               (pcase (org-export-output-file-name ".html")
                 ("index.html" "")
                 (`,else else)))))
    `(;; Absolute URLs avoid potential problems with 3rd party parsers who
      ;; don’t resolve relative URLs against the base URL. E.g. the parser
      ;; that I myself wrote for other people’s h-entry posts... >.>
      :html-link-home ,home
      :b0-page-url ,url
      :with-author nil
      :with-creator nil ;; This is handled by me in my template don’t you worry
      :section-numbers nil
      :with-date t
      :time-stamp-file nil ;; The "Created" in postamble
      ;; Do this myself
      :with-title nil
      ;; Sometimes this is useful but not always
      :html-link-use-abs-url nil
      :with-smart-quotes t
      ;; I don’t think I use this but ok
      :html-head-include-scripts t
      ;; This obviously needs to go
      :html-head-include-default-style t
      :html-html5-fancy t
      ;; Amazing
      :with-tex t
      :html-equation-reference-format "\eqref{%s}"
      :html-doctype "html5"
      ;; This fixes pages with very long non-breaking lines (code) from
      ;; looking completely zoomed out on Chrome.
      ;; https://developer.mozilla.org/en-US/docs/Web/HTML/Viewport_meta_tag
      :html-viewport ((width "device-width")
                      (initial-scale "1")
                      (minimum-scale "0.86")
                      (maximum-scale "5"))
      ;; Completely not what this is for, but whatever man.
      :html-validation-link ,b0-page//footer
      ;; Author info etc must go inside the <article>, which is content in org
      ;; speak. I have nothing to put in the postamble. The whole <body> level
      ;; templating system in ox-html is quite primitive. Ignore it.
      :html-preamble nil
      :html-link-up ""
      :html-home/up-format ,(xmlgen '(nav :id "org-div-home-and-up"
                                          ;; The first %s is html-link-up which is empty
                                          "%s" (a :href "%s" "Home")))
      :html-head ,(b0-page//xmlgens
                   `((link :rel "stylesheet" :href "/style.min.css")
                     (meta :name "color-scheme" :content "light dark")
                     (link :rel "canonical" :href ,url)
                     ,@ (when webmentionp
                          ;; Giant netlify hack, which requires this field, but
                          ;; accepts it in the query as well as in POST. Very
                          ;; lucky break or this wouldn’t have been possible at
                          ;; all.
                          '((link :rel "webmention" :href "/webmention?form-name=webmention")))))
      :html-postamble ,(when webmentionp 'b0-page//postamble)
      ,@ (pcase (plist-get options :b0-page)
           ("article"
            '(;; Until I wrap the /entire/ HTML page, ox-html will always use
              ;; this to wrap the content--I can’t opt out. So I’d rather have
              ;; my <article> top-level, here. TODO: Handle full HTML
              ;; generation and just ignore all of this.
              :html-content-class "h-entry"
              :html-divs ( (content "article" "content")
                           (preamble "div" "preamble")
                           (postamble "div" "postamble")))))
      ,@ options)))

(org-export-define-derived-backend 'b0-page 'html
  :options-alist '((:copyright-years "COPYRIGHT_YEARS" nil "2007–2023" t)
                   (:author "AUTHOR" nil "Hraban" t)
                   (:with-toc nil "toc" nil)
                   ;; Default to article because most of my files are articles
                   (:b0-page "B0_PAGE" nil "article"))
  :filters-alist '((:filter-options . b0-page//filter-options))
  :translate-alist '((inner-template . b0-page//inner-template)
                     (latex-fragment . b0-page//translate-latex)
                     (latex-environment . b0-page//translate-latex)))

(cl-defun b0-page/publish-to-html (plist filename pub-dir &optional (backend 'b0-page))
  (let ((org-html-htmlize-output-type 'css)
        ;; Just prepending doesn’t work, apparently. I also don’t think this is
        ;; strictly an alist, because some keys are allowed to be repeated, like
        ;; :var, so it probably consumes the entire list.
        (org-babel-default-header-args (b0-page//alist-new
                                        org-babel-default-header-args
                                        '((:exports . "both")
                                          (:eval . "never-export")
                                          (:results . "verbatim"))))
        (org-babel-default-inline-header-args (b0-page//alist-new
                                               org-babel-default-inline-header-args
                                               '((:exports . "code"))))
        (opts `(;; All files are relative to this directory (for export)
                :b0-src-base ,(file-truename (concat default-directory (plist-get plist :base-directory)))
                             ;; The subdirectory in which all files are published, minus
                             ;; "dist/". Point being: if project has base /a/b, publishing
                             ;; in dist/foo, then file /a/b/c/d.org will end up in
                             ;; /foo/c/d.html.
                :b0-publishing-subdir ,(b0-page//strip-prefix
                                        (plist-get plist :publishing-directory)
                                        "dist/")
                . ,plist)))
    (org-publish-org-to backend filename ".html" opts pub-dir)))

(provide 'b0-page)
